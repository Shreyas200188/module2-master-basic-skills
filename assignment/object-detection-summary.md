**Object Detection** is one of the most used and applied feature of computer vision.

## What is Object Detection?

Object detection is basically finding out the approximate location/position of various instances in a image and classifying them into respective categories.

<img src="https://miro.medium.com/max/768/1*VXZ8CamGG2Z0M0N4t0Fmng.jpeg">

## What are the Types of Objet Detection?

#### - Machine Learning Based Approach

They are used to look at various features of an image, such as the color histogram or edges, to identify groups of pixels that may belong to an object. These features are then fed into a regression model that predicts the location of the object along with its label.

#### - Deep Learning Based Approach

They  employ convolutional neural networks (CNNs) to perform end-to-end, unsupervised object detection, in which features don’t need to be defined and extracted separately. Neural network are now the state-of-the-art method for oject detection.

## How does Object Detection Work?

Deep learning-based object detection models typically have two parts. An encoder takes an image as input and runs it through a series of blocks and layers that learn to extract statistical features used to locate and label objects. Outputs from the encoder are then passed to a decoder, which predicts bounding boxes and labels for each object. There are various detectors which one can use:

#### - Pure Regressor

The regressor is connected to the output of the encoder and predicts the location and size of each bounding box directly. The limitation to this is that one has to know the number of bounded boxes beforehand and feed taht number to the algorithm to work efficiently.

#### - Region Proposal Network

In this decoder, the model proposes regions of an image where it believes an object might reside. The pixels belonging to these regions are then fed into a classification subnetwork to determine a label (or reject the proposal). It then runs the pixels containing those regions through a classification network. The benefit of this method is a more accurate, flexible model that can propose arbitrary numbers of regions that may contain a bounding box.

<img src="https://www.fritz.ai/images/rpn.jpg">

#### - Single Shot Detectors

SSDs rely on a set of predetermined regions. A grid of anchor points is laid over the input image, and at each anchor point, boxes of multiple shapes and sizes serve as regions. For each box at each anchor point, the model outputs a prediction of whether or not an object exists within the region and modifications to the box’s location and size to make it fit the object more closely. Because there are multiple boxes at each anchor point and anchor points may be close together, SSDs produce many potential detections that overlap. Post-processing must be applied to SSD outputs in order to prune away most of these predictions and pick the best one. The most popular post-processing technique is known as *non-maximum suppression*.

<img src="https://www.fritz.ai/images/ssd.jpg">

## Model Architectures Which are Used

**R-CNN Family**: Based on region proposal method
- Faster R-CNN
- Mask R-CNN

**SSD Family**: Based on single shot method
- YOLO
- SqueezeDet

**CentreNet** :  It treats objects as single points, predicting the X, Y coordinates of an object’s center and its extent (height and width). This technique has proven both more efficient and accurate than SSD or R-CNN approaches.