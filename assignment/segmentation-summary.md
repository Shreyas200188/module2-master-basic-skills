**Segmentation** is one of the most studied features nowadays in the Computer Vision field.

## What is Segmentation?

It is the process of classifying each pixel of a feeded image into a class/category.

#### The Two Types of Image Segmentation:

1. *Semantic Segmentation* - It is a process where classifying each pixel of a particular type into under one label. For example, in an image of two cats, all pixels of containing the parts of both the cats will be classified under the label, Cat.

2. *Instance Segmentation* - It classifies each pixel into a unique label. In the above stated example, the two cats will be labelled as Cats only but different cats denoted by different color/id.

<img src="https://miro.medium.com/max/2436/0*QeOs5RvXlkbDkLOy.png">

#### What Happens Behind the Segmentation Process?

The basic structure includes:

*The Encoder*: A set of layers that extract features of an image through a sequence of progressively narrower and deeper filters. Oftentimes, the encoder is pre-trained on a different task (like image recognition), where it learns statistical correlations from many images and may transfer that knowledge for the purposes of segmentation.

*The Decoder*: A set of layers that progressively grows the output of the encoder into a segmentation mask resembling the pixel resolution of the input image.

*Skip Connections*: Long range connections in the neural network that allow the model to draw on features at varying spatial scales to improve model accuracy.

#### The Model Architectures

The basic architecture includes a few convolutional and pooling layers and then a fully connected layers. By upscaling and downscaling the input map a final feature map is produced and based on that the result is given. Some of them are:

###### - U-Net

The U-Net model architecture derives its name from its U shape. The encoder and decode work together to extract salient features of an image and then use those features to determine which label should be given to each pixel. In the case of a U-Net model, the encoder is made up of blocks that downscale an image into narrower feature layers while the decoder mirrors those blocks in the opposite direction, upscaling outputs to the original image size and ultimately predicting a label for each pixel. Skip connections cut across the U to improve performance.

<img src="https://nanonets.com/blog/content/images/size/w1000/2020/08/1_f7YOaE4TWubwaFF7Z1fzNw.png">

###### - DeepLab and Mask R-CNN

They are products of Google and Facebook respectively which use a combination of encoders, decoders and skip connections and also include some special features such as atrous convolutions, spatial pyramidal pooling and CRF for improving final output.



